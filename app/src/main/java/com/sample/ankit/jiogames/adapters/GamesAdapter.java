package com.sample.ankit.jiogames.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.makeramen.roundedimageview.RoundedImageView;
import com.sample.ankit.jiogames.R;
import com.sample.ankit.jiogames.databinding.LayoutGamesRowBinding;
import com.sample.ankit.jiogames.interfaces.OnItemClickedListener;
import com.sample.ankit.jiogames.models.Game;
import com.sample.ankit.jiogames.models.Item;
import com.sample.ankit.jiogames.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class GamesAdapter extends RecyclerView.Adapter<GamesAdapter.ItemClass> {
   private LayoutGamesRowBinding binding;
   private List<Item> games = new ArrayList<>();
    private Context context;
    private OnItemClickedListener onItemClickedListener;
    public GamesAdapter(Context context, List<Item> games) {
        this.games = games;
        this.context = context;
        onItemClickedListener = (OnItemClickedListener) context;
    }

    @NonNull
    @Override
    public ItemClass onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_game_item, parent, false);
        return new ItemClass(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemClass holder, int position) {
        final Item gameItem = games.get(position);
        holder.textViewTitle.setText(gameItem.getTitle());
        holder.textViewCompanyName.setText(gameItem.getCompanyName());
        final RoundedImageView imageView = holder.imageViewGame;
        Glide.with(holder.itemView.getContext())
                .asBitmap()
                .load(gameItem.getImageUrl())
                .into(imageView);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickedListener.onItemClick(gameItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return games.size();
    }

    public class ItemClass extends RecyclerView.ViewHolder {
        public TextView textViewTitle;
        public TextView textViewCompanyName;
        public RoundedImageView imageViewGame;
        public ItemClass(View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.text_view_game_title);
            textViewCompanyName = itemView.findViewById(R.id.text_view_company_name);
            imageViewGame = itemView.findViewById(R.id.image_view_game);
        }
    }
}
