package com.sample.ankit.jiogames.adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.sample.ankit.jiogames.R;
import com.sample.ankit.jiogames.models.Carousel;

import java.util.ArrayList;
import java.util.List;


public class CarouselAdapter extends PagerAdapter {
    private final Context context;
    private final LayoutInflater inflater;
    private List<Carousel> carousels = new ArrayList<>();
    public CarouselAdapter(Context context, List<Carousel> carousels) {
        this.context = context;
        this.carousels=carousels;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }


    @Override
    public int getCount() {
        return carousels.size();
    }
    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.layout_images_sliding, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.sliding_image);

        Glide.with(context).load(carousels.get(position).getImageUrl()).into(imageView);
//        imageView.setImageResource(carousels.get(position).getImageUrl());

        view.addView(imageLayout, 0);

        return imageLayout;
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}
