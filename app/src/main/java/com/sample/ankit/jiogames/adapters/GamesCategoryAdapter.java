package com.sample.ankit.jiogames.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sample.ankit.jiogames.R;
import com.sample.ankit.jiogames.databinding.LayoutGamesRowBinding;
import com.sample.ankit.jiogames.models.Game;

import java.util.ArrayList;
import java.util.List;

public class GamesCategoryAdapter extends RecyclerView.Adapter<GamesCategoryAdapter.CategoryClass> {
   private LayoutGamesRowBinding binding;
   private List<Game> games = new ArrayList<>();
    private Context context;
    public GamesCategoryAdapter(Context context,List<Game> games) {
        this.games = games;
        this.context = context;
    }

    @NonNull
    @Override
    public CategoryClass onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_games_row, parent, false);
        binding = DataBindingUtil.bind(itemView);
        return new CategoryClass(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryClass holder, int position) {
        Game game = games.get(position);
        holder.textViewTitle.setText(game.getTitle());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(holder.itemView.getContext().getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
        holder.gamesRecyclerView.setLayoutManager(mLayoutManager);
        holder.gamesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        GamesAdapter gamesAdapter = new GamesAdapter(this.context,game.getItems());
        holder.gamesRecyclerView.setAdapter(gamesAdapter);
    }

    @Override
    public int getItemCount() {
        return games.size();
    }

    public class CategoryClass extends RecyclerView.ViewHolder {
        public TextView textViewTitle;
        public RecyclerView gamesRecyclerView;
        public CategoryClass(View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.text_view_category_title);
            gamesRecyclerView = itemView.findViewById(R.id.games_recycler_view);
        }
    }
}
