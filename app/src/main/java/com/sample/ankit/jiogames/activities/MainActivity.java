package com.sample.ankit.jiogames.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedImageView;
import com.sample.ankit.jiogames.R;
import com.sample.ankit.jiogames.adapters.CarouselAdapter;
import com.sample.ankit.jiogames.adapters.GamesCategoryAdapter;
import com.sample.ankit.jiogames.databinding.ActivityMainBinding;
import com.sample.ankit.jiogames.interfaces.OnItemClickedListener;
import com.sample.ankit.jiogames.models.BaseDataModel;
import com.sample.ankit.jiogames.models.Game;
import com.sample.ankit.jiogames.models.Item;
import com.sample.ankit.jiogames.transformers.ZoomOutPageTransformer;
import com.sample.ankit.jiogames.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,OnItemClickedListener, View.OnClickListener {
    private static final int RATIO_SCALE = 2;
    private  final String TAG = this.getClass().getSimpleName();
    private ActivityMainBinding activityMainBinding;
    private GamesCategoryAdapter gamesCategoryAdapter;
    private RecyclerView gamesCategoryRecyclerView;
    private ViewPager carouselPager;
    private List<Game> games = new ArrayList<>();
    private TextView textViewDescritpion,textViewTotalDownlods,textViewHighestScore;
    private RoundedImageView imageViewGame;
    private LinearLayout infoLayout;
    private RelativeLayout blankLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        gamesCategoryRecyclerView = findViewById(R.id.games_category_recycler_view);
        carouselPager = findViewById(R.id.carousel);
        textViewDescritpion = findViewById(R.id.description);
        textViewHighestScore = findViewById(R.id.highest_score);
        textViewTotalDownlods = findViewById(R.id.total_downloads);
        imageViewGame = findViewById(R.id.gameImageView);
        infoLayout = findViewById(R.id.info_layout);
        blankLayout =findViewById(R.id.blank_layout);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        gamesCategoryRecyclerView.setLayoutManager(mLayoutManager);
        gamesCategoryRecyclerView.setItemAnimator(new DefaultItemAnimator());
        blankLayout.setOnClickListener(this);
        initData();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void initData() {
        Gson gson = new Gson();
        final BaseDataModel baseDataModel = gson.fromJson(Utils.loadJsonFromAssets(this,getString(R.string.file_sample_json)),BaseDataModel.class);
        gamesCategoryAdapter = new GamesCategoryAdapter(this,baseDataModel.getGames());
        gamesCategoryRecyclerView.setAdapter(gamesCategoryAdapter);

        CarouselAdapter mPagerAdapter = new CarouselAdapter(this,baseDataModel.getCarousels());
        carouselPager.setAdapter(mPagerAdapter);
        carouselPager.setPageTransformer(true, new ZoomOutPageTransformer());
        carouselPager.setPageMargin(-Utils.convertDipToPixels(this,70));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onItemClick(Item game) {

        textViewDescritpion.setText(game.getDescription());
        textViewTotalDownlods.setText(game.getTotalDownloads());
        textViewHighestScore.setText(game.getHighestScore());
        Glide.with(this).load(game.getImageUrl()).into(imageViewGame);
        infoLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.blank_layout:
                if(infoLayout.getVisibility() == View.VISIBLE)
                    infoLayout.setVisibility(GONE);
                break;
        }
    }
}
