package com.sample.ankit.jiogames.interfaces;

import com.sample.ankit.jiogames.models.Item;

public interface OnItemClickedListener {
    public void onItemClick(Item game);

}
