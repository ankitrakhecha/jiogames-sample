
package com.sample.ankit.jiogames.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseDataModel {

    @SerializedName("carousels")
    @Expose
    private List<Carousel> carousels = null;
    @SerializedName("games")
    @Expose
    private List<Game> games = null;

    public List<Carousel> getCarousels() {
        return carousels;
    }

    public void setCarousels(List<Carousel> carousels) {
        this.carousels = carousels;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

}
