
package android.databinding;
import com.sample.ankit.jiogames.BR;
class DataBinderMapper  {
    final static int TARGET_MIN_SDK = 15;
    public DataBinderMapper() {
    }
    public android.databinding.ViewDataBinding getDataBinder(android.databinding.DataBindingComponent bindingComponent, android.view.View view, int layoutId) {
        switch(layoutId) {
                case com.sample.ankit.jiogames.R.layout.activity_main:
                    return com.sample.ankit.jiogames.databinding.ActivityMainBinding.bind(view, bindingComponent);
                case com.sample.ankit.jiogames.R.layout.layout_games_row:
                    return com.sample.ankit.jiogames.databinding.LayoutGamesRowBinding.bind(view, bindingComponent);
        }
        return null;
    }
    android.databinding.ViewDataBinding getDataBinder(android.databinding.DataBindingComponent bindingComponent, android.view.View[] views, int layoutId) {
        switch(layoutId) {
        }
        return null;
    }
    int getLayoutId(String tag) {
        if (tag == null) {
            return 0;
        }
        final int code = tag.hashCode();
        switch(code) {
            case 423753077: {
                if(tag.equals("layout/activity_main_0")) {
                    return com.sample.ankit.jiogames.R.layout.activity_main;
                }
                break;
            }
            case 1533388157: {
                if(tag.equals("layout/layout_games_row_0")) {
                    return com.sample.ankit.jiogames.R.layout.layout_games_row;
                }
                break;
            }
        }
        return 0;
    }
    String convertBrIdToString(int id) {
        if (id < 0 || id >= InnerBrLookup.sKeys.length) {
            return null;
        }
        return InnerBrLookup.sKeys[id];
    }
    private static class InnerBrLookup {
        static String[] sKeys = new String[]{
            "_all"};
    }
}